# Learning Log

<h3>A demo project about how to use Django, Bootstrap, and deploy the application.</h3>

<h4>Specification</h4>
<p>We'll write a web app called Learning Log that allows users to log the topics they're interested in and to make journal entries as they learn about each topic. The Learning Log home page should describe the site and invite users to either register or log in. Once logged in, a user should be able to create new topics, add entries, and read edit existing entries.</p>

<h4>Materials Used</h4>
<ul>
    <li>Operating System: Ziron OS (Linux)</li>
    <li>Framework / library: Django, SQLite</li>
    <li>IDE: Pycharm</li>
    <li>Linux Command Line</li>
    <li>Front-end: Bootstrap4-Django, jQuery, Jinja2 (Template Engine)</li>
    <li>Deployment: TBD</li>
</ul>

<h4>Process of setting up the project</h4>
<ol type="1">
    <li>Open and Start a project in Git (Gitlab.com).</li>
    <li>Wrote the project name in a blank template.</li>
    <li>Once loaded, grabbed the Git URL</li>
    <li>Open PyCharm</li>
    <ol type="i">
        <li>Goto VCS &rarr; Checkout Version Control &rarr; Git</li>
        <li>Pass URL into TextBox.</li>
        <li>Press "Test" to make sure the connection is there.</li>
        <li>Press "Clone" to clone the project.</li>
    </ol>
    <li>Go to Terminal</li>
    <ol type="i">
        <li>&rarr; type "sudo apt-get update"</li>
        <li>&rarr; type "sudo apt install python3-pip"</li>
        <li>&rarr; type "pip3 install pipenv" (Recommended package manager now, and not "pip install")</li>
        <li>&rarr; type "pipenv install django" (This will install the Django framework.)</li>
        <li>&rarr; type "pipenv shell" (This means you are into the Django shell.)</li>
        <li>&rarr; type "django-admin startproject learning_log ." (Make sure you are using only Use letters, numbers, and underscores in project name. Also add a space between project name and the period (.).</li>
        <li>&rarr; type "python manage.py runserver" (This will run the the development server, and you will see the beginning template of Django. See screenshots folder to understand Django's beginning template.)</li>
        <li><strong>Warning:</strong> you will recieve this message. <strong>"You have 15 unapplied migration(s). Your project may not work properly until you apply the migrations for app(s): admin, auth, contenttypes, sessions.
Run 'python manage.py migrate' to apply them."</strong> It is nothing to worry, but it is good idea to apply the migrations.</li>
    </ol>
    <li>To quit the development server, press "Control-C (CTRL+C)</li>
    <li><strong>Note:</strong> You will have to restart the server each time you make a python change. If you make a HTML or Jinja2 change, all you will need to do is refresh the page.</li>
    <li>Back into the Termainal</li>
    <ol type="i">
        <li>&rarr; type "python manage.py migrate" This will migrate all the Django libraries.</li>
    </ol>
</ol>

<h4>Process of the Learning Log project</h4>
<p>This displays the importance of the tasks that I did that day.</p>

<h5>Date: 2018-08-14</h5>
<ul>
    <li>Started the project app (learning logs) to Django environment.</li>
    <li>Updated (learning-log/learning-log/settings.py) to import learning-logs installed apps list, so it will inform the whole project base.</li>
    <li>Built two tables Topic and Entry, respectively. Used migrations to build the SQLite database for storing the data.</li>
    <li>Added the two tables to admin.py, so the administrator site will be able to write logs for the users and sample data.</li>
    <li>Added screenshots to the screenshots folder to view the photos without going into the site completely.</li>
    <li>Started working on routing the URLs and views to make the home page visible to the whole project.</li>
</ul>

<h5>Date: 2018-08-16</h5>
<ul>
    <li>Worked on routing the URLs and views to make the home page visible.</li>
    <li>Wrote templates, so there will be data on the web page.</li>
    <li>Add screenshots to the screenshots folder to view.</li>
    <li>Beautified the urls, so that it is easy to read for humans.</li>
    <li>Added several templates to view the topics and entries.</li>
    <li>Added 'templates' to the directories in settings.py to be able to view content.</li>
    <li>Worked with Django ORM to show the content in order by date added.</li>
    <li>Used Jinja to display the content onto the HTML template for viewing.</li>
</ul>

<h5>Date: 2018-08-19</h5>
<ul>
    <li>Added screenshots to the screenshots folder to view.</li>
    <li>Added new entry, new topic, and edit entry for the user to write their own logs.</li>
    <li>Wrote forms to the users for they can write their own logs.</li>
    <li>Added new app to the project 'users' to authenticate the users.</li>
</ul>

<h5>Date: 2018-08-20</h5>
<ul>
    <li>Built an authentication system for creating accounts.</li>
    <li>Added secrity to the login, so only the person who is logged in can view their own data.</li>
    <li>Added screenshots to the screenshots folder to view.</li>
</ul>

<h5>Date: 2018-08-21</h5>
<ul>
    <li>Added HTTP 404 if something happens to the page that can't be displayed.</li>
    <li>Added login required to view the user's data.</li>
</ul>