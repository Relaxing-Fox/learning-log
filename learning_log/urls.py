from django.conf.urls import include
from django.contrib import admin
from django.urls import path

app_name = 'learning_log'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('users.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('', include('learning_logs.urls'))
]
