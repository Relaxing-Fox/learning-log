""" Defines url patterns for registration."""

# import django.contrib.auth.views
from django.contrib.auth.views import LoginView
from django.urls import path

from . import views

urlpatterns = [
    # Login page.
    path('login/', LoginView.as_view(), name='login'),

    # Logout page.
    path('logout/', views.logout_view, name='logout'),

    # Registration page.
    path('register/', views.register, name='register'),
]
